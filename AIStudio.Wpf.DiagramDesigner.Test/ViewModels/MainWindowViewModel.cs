﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace AIStudio.Wpf.DiagramDesigner.Test.ViewModels
{
    class MainWindowViewModel : BindableBase
    {
        public ToolBoxViewModel ToolBoxViewModel
        {
            get; private set;
        }

        public DiagramViewModel DiagramViewModel
        {
            get; private set;
        }

        protected IDiagramServiceProvider _service
        {
            get
            {
                return DiagramServicesProvider.Instance.Provider;
            }
        }

        public MainWindowViewModel()
        {
            ToolBoxViewModel = new ToolBoxViewModel();
            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.ShowGrid = true;
            DiagramViewModel.GridCellSize = new Size(100, 60);
            DiagramViewModel.CellHorizontalAlignment = CellHorizontalAlignment.Center;
            DiagramViewModel.CellVerticalAlignment = CellVerticalAlignment.Center;
            DiagramViewModel.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.PageSize = new Size(double.NaN, double.NaN);

            ConnectorViewModel.PathFinder = new OrthogonalPathFinder();
            _service.DrawModeViewModel.VectorLineDrawMode = DrawMode.BoundaryConnectingLine;
        }
    }
    
}
