﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Helpers;
using AIStudio.Wpf.Flowchart;
using AIStudio.Wpf.Flowchart.Models;
using AIStudio.Wpf.Flowchart.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Test.ViewModels
{
    public class ToolBoxViewModel
    {
        private List<ToolBoxData> toolBoxItems = new List<ToolBoxData>();

        public ToolBoxViewModel()
        {
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.Start, typeof(StartFlowNode), 80, 48));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.End, typeof(EndFlowNode), 80, 48));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.Middle, typeof(MiddleFlowNode), 80, 48));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.Decide, typeof(DecideFlowNode), 80, 48));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.COBegin, typeof(COBeginFlowNode), 48, 48));
            toolBoxItems.Add(new FlowchartToolBoxData(NodeKinds.COEnd, typeof(COEndFlowNode), 80, 48));
        }

        public List<ToolBoxData> ToolBoxItems
        {
            get
            {
                return toolBoxItems;
            }
        }
    }  
   
}
