﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum ArrowPathStyle
    {
        None,
        Arrow1,
        Arrow2,
        Arrow3,
        Arrow4,
        Arrow5,
        Arrow6,
        Arrow7,
        Arrow8,
        Arrow9,
        Arrow10,
        Arrow11,
    }
}
