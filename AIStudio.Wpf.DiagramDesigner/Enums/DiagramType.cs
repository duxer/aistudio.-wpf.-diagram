﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum DiagramType
    {
        Normal,
        FlowChart,
        Logical,
        SFC,
    }
}
