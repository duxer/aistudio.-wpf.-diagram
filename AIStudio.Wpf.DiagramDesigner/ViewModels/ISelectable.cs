﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }
}
