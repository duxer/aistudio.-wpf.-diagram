﻿using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.DiagramHelper.Extensions.Models
{
    public class PathDesignerItem : DesignerItemBase
    {
        public PathDesignerItem()
        {

        }

        public PathDesignerItem(DesignerItemViewModelBase item) : base(item) { }
    }
}
