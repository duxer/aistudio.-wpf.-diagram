﻿using System.Xml.Serialization;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramHelper.Extensions.ViewModels;

namespace AIStudio.Wpf.DiagramHelper.Extensions.Models
{
    public class PersistDesignerItem : DesignerItemBase
    {       
        public PersistDesignerItem()
        {

        }
        public PersistDesignerItem(PersistDesignerItemViewModel item) : base(item)
        {
            this.HostUrl = item.HostUrl;
        }

        [XmlAttribute]
        public string HostUrl { get; set; }
    }
}
