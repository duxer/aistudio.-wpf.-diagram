﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramHelper.Extensions.ViewModels;

namespace AIStudio.Wpf.DiagramHelper.Extensions.Models
{
    public class SettingsDesignerItem : DesignerItemBase
    {
        public SettingsDesignerItem()
        {

        }
        public SettingsDesignerItem(SettingsDesignerItemViewModel item) : base(item)
        {
            this.Setting = item.Setting;
        }

        public string Setting { get; set; }
    }
}
