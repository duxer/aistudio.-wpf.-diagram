﻿using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.DiagramHelper.Extensions.ViewModels
{
    public class SvgDesignerItemViewModel: MediaItemViewModel
    {
        protected override string Filter { get; set; } = "Svg|*.svg";

        public SvgDesignerItemViewModel() : base()
        {
        }

        public SvgDesignerItemViewModel(IDiagramViewModel parent, MediaDesignerItem designer) : base(parent, designer)
        {

        }
    }
}
