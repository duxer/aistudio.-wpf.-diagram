﻿using System;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Helpers;

namespace AIStudio.Wpf.SFC.Models
{
    public class SFCToolBoxData : ToolBoxData
    {
        public SFCNodeKinds Kind
        {
            get; set;
        }
        public SFCToolBoxData(SFCNodeKinds kind, Type type, double width = 32, double height = 32) : base(null, null, type, width, height)
        {
            Kind = kind;
            ColorViewModel.LineColor.Color = Colors.Gray;
            ColorViewModel.FillColor.Color = Colors.Blue;

        }
    }
}
